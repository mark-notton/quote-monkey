<!DOCTYPE html>
<html>
  <head>
    <title>Quote Monkey</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
  </head>
  <body style="padding:0; margin:0;">
    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/logo.svg'); ?>
    <script>
      document.addEventListener("DOMContentLoaded", function(event) {
        if ((( !!window.ActiveXObject && +( /msie\s(\d+)/i.exec( navigator.userAgent )[1] ) ) || NaN) <= 9) {
          var logo = document.getElementById("logo");
          var fallback = document.createElement("div");
          fallback.className = "fallback";
          fallback.id = "logo";
          logo.parentNode.replaceChild(fallback, logo);
        }
      });
    </script>
  </body>
</html>
