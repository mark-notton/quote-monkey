var gulp	    	     = require('gulp'),
		chalk		         = require('chalk'),
		gulpif           = require('gulp-if'),
		sass		         = require('gulp-sass'),
		notify           = require('gulp-notify'),
		plumber          = require('gulp-plumber'),
		sourcemaps       = require('gulp-sourcemaps'),
		browserSync      = require('browser-sync').create();

// Settings
var project			     = 'Quote Monkey';
var localServer      = 'www.quotemonkey.loc';
var sourceMaps 	 	 	 = false;

// Success and Error notices handlers
var handlers = {
  onError: function() {
    return notify.onError({
      'icon': "https://i.imgur.com/VsfiLjV.png",
      'title': project,
      'message': '<%= error.message %>'
    });
  },
  onSuccess: function(message) {
    return notify({
      'icon': "https://i.imgur.com/G6fTWAs.png",
      'title': project,
      'message': message || 'Successful',
      'onLast': true
    });
  },
};

// Watcher tasks
gulp.task('serve', ['sass'], function() {

	browserSync.init({
		watchTask: true,
		open: "external",
		proxy: localServer,
    host: localServer,
		notify: false,
		port:3000
	});

	gulp.watch(['sass/**/*.scss'], ['sass']);
	gulp.watch('*.php').on('change', browserSync.reload);
	gulp.watch('*.svg').on('change', browserSync.reload);
});

// Sass
gulp.task('sass', function() {
	return gulp.src(["sass/**/*.scss"])
	.pipe(gulpif(sourceMaps, sourcemaps.init()))
	.pipe(plumber({errorHandler: handlers.onError() }))
	.pipe(sass({outputStyle: 'compact'}))
	// .pipe(sass({outputStyle: 'nested'}))
	.pipe(gulpif(sourceMaps, sourcemaps.write()))
	.pipe(gulp.dest('css/'))
	.pipe(browserSync.stream())
	.pipe(handlers.onSuccess('SASS Compiled Successfully') );
});
